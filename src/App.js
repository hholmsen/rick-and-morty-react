import React, { useState } from 'react';
import './App.css';
import CharacterList from "./components/CharacterList/CharacterList"
import Character from "./components/Character/Character"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"


function App() {
  const [message, setState]=useState();

  const callbackFunction = (childData) => {
    console.log(childData)
    setState(childData)
  }

  return (
   
    <div className="App">
       <Router>

         <Switch>
         <Route path="/character"><Character data={message}/></Route>
            <Route path="/"><CharacterList parentCallback={callbackFunction}/></Route>
          </Switch>
       </Router>
    </div>
   
  );
}

export default App;
// 