import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"

class Character extends React.Component{

     

    render(){
        
        return (
            
                
            <div >
                <img src={this.props.data.image} alt={this.props.data.name}></img>
                <h1>Name:{this.props.data.name} </h1>
                <h2>Id: {this.props.data.id} </h2>
                <h2>Species: {this.props.data.species}</h2>
                <h2>Status: {this.props.data.status}</h2>
                <h2>Origin: {this.props.data.origin.name}</h2>
                <br/>
                <br/>
                <Link to="/">
                    <button >
                    Return
                </button>
            </Link>
            
            </div>
            


        )
    }
}

export default Character;

//