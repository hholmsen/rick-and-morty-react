import React from 'react'
import styles from "./CharacterList.module.css"
import Character from '../Character/Character.js'
import mortu from '../../Assets/morty.png'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"
import { getNodeText } from '@testing-library/react'

class CharacterList extends React.Component{
    
    state ={
        characters:[],
        page:1,
        link:'https://rickandmortyapi.com/api/character/?page=',
        next:"https://rickandmortyapi.com/api/character/"
    }
    componentDidMount(){
        fetch(this.state.next).then(resp => resp.json()).then(resp=> {
            
            const chars=[...this.state.characters]
            chars.push(...resp.results)
            this.setState({
                characters: chars,
                next: resp.info.next
            })
            console.log(this.state.next)
        }).catch(err=>console.error(err)
        )
    }


    async setLink(e){

        console.log(e.target.value)
        await this.setState({
            characters:[],
            link: 'https://rickandmortyapi.com/api/character/?name='+e.target.value
        })
        console.log(this.state.link)
        fetch(this.state.link)
        .then(resp=>resp.json()).then(resp=>{
            if(resp.results){
            const chars=[...this.state.characters]

            chars.push(...resp.results)
            this.setState({
                characters: chars
            })
        }else {
           /* this.setState({
                characters: {
                    "character":[
                        {"name":"The search yielded 0 results"},
                        {"image":"https://vignette.wikia.nocookie.net/rickandmorty/images/e/ef/Vlcsnap-2015-01-31-02h46m26s111.png/revision/latest/scale-to-width-down/340?cb=20150131104650"}
                    ]
                }
            })
            console.log(this.state.characters)
            */
        }
        }).catch(err=>console.error(err))

    }
    handleClick(){

        fetch(this.state.next).then(resp => resp.json()).then(resp=> {
            
            const chars=[...this.state.characters]
            chars.push(...resp.results)
            this.setState({
                characters: chars,
                next: resp.info.next
            })
            
        }).catch(err=>console.error(err) 
        )
    }
    handleClcko(c){
        this.callback(c)
    }

    callback(c){
        console.log(c)
        this.props.parentCallback(c);
    }
    render(){
        
        const characters=this.state.characters.map(character=>{
            return(
     
                   
                <div className="character">
                <Link to="/character">
                    <button onClick={()=> this.handleClcko(character)}>
                <img src={character.image} alt={character.name}></img>
                    <h4>{character.name} </h4>
                    

                    </button>
                    </Link>
                    
                <br/>
                <br/>
                <br/>
                <br/>
                </div>


            )
        })
        return(
        <div className={styles.CharacterList}>
           
            <img src={mortu} alt="mortu"></img>
            <br/>
            <input type="text" placeholder=" search" id="searchbar" onChange={e => this.setLink(e)}></input>
            <br/>
            <br/><br/><br/>
            <div className="characters">
                    {characters}
                    </div>
                    <br></br>
        <button onClick={() => this.handleClick()}>Load More</button>  
        </div>
       
        )
    }
}
//

export default CharacterList;